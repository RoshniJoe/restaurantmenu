//
//  GenericFunctions.swift
//  RestaurantMenu
//
//  Created by Roshni Varghese on 2019-09-11.
//  Copyright © 2019 RoshniJoe. All rights reserved.
//

import UIKit

class GenericFunctions: NSObject {
    
    //MARK:- Alert View Controller
    static  func showAlertView(targetVC: UIViewController, title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        targetVC.present(alert, animated: true, completion: nil)
        let when = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: when){
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
    static func okAlert(targetVC: UIViewController, title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        let okalert = UIAlertAction (title: "OK", style:
            UIAlertAction.Style.default, handler: { UIAlertActionStyle in
                alert.dismiss(animated: true, completion: nil)
        })
        alert.addAction(okalert)
        targetVC.present(alert, animated: true, completion: nil)
        
    }
    
    
    //MARK:- Alert View Controller
    static  func showAlertViewWithOptions(targetVC: UIViewController, title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction((UIAlertAction(title: "Yes", style: .default, handler: {(action) -> Void in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SwitchToOtherProfile"), object: nil)
        })))
        alert.addAction((UIAlertAction(title: "No", style: .cancel, handler: {(action) -> Void in
        })))
        targetVC.present(alert, animated: true, completion: nil)
    }
    
    
    static func alertAndPop(targetVC: UIViewController, title: String, message: String) {
        
        let alertVC =  UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let okBtn = UIAlertAction.init(title: "OK", style: .cancel) { (action) in
            targetVC.navigationController?.popViewController(animated: false)
        }
        alertVC.addAction(okBtn)
        targetVC.present(alertVC, animated: false, completion: nil)
    }
}
