//
//  MenuItemTableViewCell.swift
//  RestaurantMenu
//
//  Created by Roshni Varghese on 2019-09-12.
//  Copyright © 2019 RoshniJoe. All rights reserved.
//

import UIKit

class MenuItemTableViewCell: UITableViewCell {

    @IBOutlet weak var imageviewItem : UIImageView!
    @IBOutlet weak var labelItem: UILabel!
    @IBOutlet weak var labelPrice: UILabel!

}
