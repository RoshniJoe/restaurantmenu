//
//  CategoryCollectionViewCell.swift
//  TouchCore
//
//  Created by Roshni Varghese on 2019-09-07.
//  Copyright © 2019 RoshniJoe. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageviewCategory : UIImageView!
    @IBOutlet weak var labelCategory : UILabel!
    @IBOutlet weak var buttonEdit: UIButton!
    @IBOutlet weak var buttonDelete: UIButton!
}
