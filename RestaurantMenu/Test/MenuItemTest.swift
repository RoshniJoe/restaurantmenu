//
//  MenuItemTest.swift
//  RestaurantMenuTests
//
//  Created by Roshni Varghese on 2019-09-14.
//  Copyright © 2019 RoshniJoe. All rights reserved.
//

import XCTest
import CoreData
@testable import RestaurantMenu

class MenuItemTest: XCTestCase {
    
    var persistentContainer: NSPersistentContainer!
    
    var managedObjectModel: NSManagedObjectModel = {
        let managedObjectModel = NSManagedObjectModel.mergedModel(from: [Bundle.main])!
        return managedObjectModel
    }()
    
    lazy var mockPersistantContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "RestaurantMenu", managedObjectModel: self.managedObjectModel)
        let description = NSPersistentStoreDescription()
        description.type = NSInMemoryStoreType
        description.shouldAddStoreAsynchronously = false
        
        container.persistentStoreDescriptions = [description]
        container.loadPersistentStores { (description, error) in
            // Check if the data store is in memory
            precondition( description.type == NSInMemoryStoreType )
            
            // Check if creating container wrong
            if let error = error {
                fatalError("In memory coordinator creation failed \(error)")
            }
        }
        return container
    }()
    
    lazy var backgroundContext: NSManagedObjectContext = {
        return self.persistentContainer.newBackgroundContext()
    }()
    
    override func setUp() {
        self.persistentContainer = mockPersistantContainer
        self.persistentContainer.viewContext.automaticallyMergesChangesFromParent = true
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testCheckEmpty() {
        let request: NSFetchRequest<MenuItems> = MenuItems.fetchRequest()
        let results = try? persistentContainer.viewContext.fetch(request)
        let rows = results ?? [MenuItems]()
        XCTAssertEqual(rows.count, 0)
    }

    func testInsert() {
        let request: NSFetchRequest<MenuItems> = MenuItems.fetchRequest()
        var results = try? persistentContainer.viewContext.fetch(request)
        let rowsBefore = results ?? [MenuItems]()
        XCTAssertEqual(rowsBefore.count, 0)
        
       let menuItem = NSEntityDescription.insertNewObject(forEntityName: "MenuItems", into: backgroundContext) as? MenuItems
        
        menuItem!.name = "Test1 Item"
        menuItem!.image = #imageLiteral(resourceName: "breakfast").pngData() as NSData?
        menuItem!.price = 3.0
        if backgroundContext.hasChanges {
            do {
                try backgroundContext.save()
            } catch {
                print("Save error \(error)")
            }
        }
        
        results = try? persistentContainer.viewContext.fetch(request)
        let rowsAfter = results ?? [MenuItems]()
        XCTAssertEqual(rowsAfter.count, 1)
    }
    
    func testUpdate() {
        testInsert()
        let request: NSFetchRequest<MenuItems> = MenuItems.fetchRequest()
        request.predicate = NSPredicate(format: "name = %@", "Test1 Item")
        var results = try? persistentContainer.viewContext.fetch(request)
        let rowsBeforeUpdate = results ?? [MenuItems]()
        XCTAssertEqual(rowsBeforeUpdate.count, 1)
        results![0].name = "Test2 Item"
        if backgroundContext.hasChanges {
            do {
                try backgroundContext.save()
            } catch {
                print("Save error \(error)")
            }
        }
        request.predicate = NSPredicate(format: "name = %@", "Test2 Item")
        results = try? persistentContainer.viewContext.fetch(request)
        let rowsAfterUpdate = results ?? [MenuItems]()
        XCTAssertEqual(rowsAfterUpdate.count, 1)
    }
    
    func testDelete(){
        testInsert()
        let request: NSFetchRequest<MenuItems> = MenuItems.fetchRequest()
        request.predicate = NSPredicate(format: "name = %@", "Test1 Item")
        var results = try? persistentContainer.viewContext.fetch(request)
        let rowsBeforeDelete = results ?? [MenuItems]()
        XCTAssertEqual(rowsBeforeDelete.count, 1)
        persistentContainer.viewContext.delete(results![0])
        if backgroundContext.hasChanges {
            do {
                try backgroundContext.save()
            } catch {
                print("Save error \(error)")
            }
        }
        request.predicate = NSPredicate(format: "name = %@", "Test1 Item")
        results = try? persistentContainer.viewContext.fetch(request)
        let rowsAfterDelete = results ?? [MenuItems]()
        XCTAssertEqual(rowsAfterDelete.count, 0)
    }
}
