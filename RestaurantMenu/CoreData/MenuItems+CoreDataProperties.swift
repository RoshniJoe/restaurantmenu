//
//  MenuItems+CoreDataProperties.swift
//  RestaurantMenu
//
//  Created by Roshni Varghese on 2019-09-10.
//  Copyright © 2019 RoshniJoe. All rights reserved.
//
//

import Foundation
import CoreData


extension MenuItems {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MenuItems> {
        return NSFetchRequest<MenuItems>(entityName: "MenuItems")
    }

    @NSManaged public var name: String?
    @NSManaged public var image: NSData?
    @NSManaged public var price: Double
    @NSManaged public var main: MainMenu?

}
