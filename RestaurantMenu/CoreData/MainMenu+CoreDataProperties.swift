//
//  MainMenu+CoreDataProperties.swift
//  RestaurantMenu
//
//  Created by Roshni Varghese on 2019-09-10.
//  Copyright © 2019 RoshniJoe. All rights reserved.
//
//

import Foundation
import CoreData


extension MainMenu {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MainMenu> {
        return NSFetchRequest<MainMenu>(entityName: "MainMenu")
    }

    @NSManaged public var name: String?
    @NSManaged public var image: NSData?
    @NSManaged public var item: MenuItems?

}
