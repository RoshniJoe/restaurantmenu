//
//  MenuCategoryCollectionViewController.swift
//  TouchCore
//
//  Created by Roshni Varghese on 2019-09-07.
//  Copyright © 2019 RoshniJoe. All rights reserved.
//

import UIKit
import CoreData

private let reuseIdentifier = "MainMenuCell"

class MainMenuCollectionViewController: UICollectionViewController , UICollectionViewDelegateFlowLayout {

    var arrayCategory = [MainMenu]()
    var entity : NSEntityDescription!
    var mainMenu: MainMenu!
    var managedObjectContext : NSManagedObjectContext!
    var fetchRequest : NSFetchRequest<NSFetchRequestResult>!
    
    //MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBar()
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        managedObjectContext = appDelegate.persistentContainer.viewContext
        entity = NSEntityDescription.entity(forEntityName: "MainMenu", in: managedObjectContext)
        fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "MainMenu")
    }
    
    override func viewWillAppear(_ animated: Bool) {
       retrieveData()
    }
    
    //MARK: - Design Nav Bar
    
    func setUpNavigationBar(){
        let titleLabel = UILabel()
        titleLabel.text = "Main Menu"
        titleLabel.font =  UIFont(name: "Arial-BoldMT", size: 17.0)
        titleLabel.textColor = #colorLiteral(red: 0.3647058824, green: 0.3607843137, blue: 0.3647058824, alpha: 1)
        titleLabel.textAlignment = .center
        let titlewidth = titleLabel.intrinsicContentSize.width
        titleLabel.frame = CGRect(x: 0, y: 0, width: titlewidth, height: 20)
        self.navigationItem.titleView = titleLabel
        let btnadd     = UIButton.init(type: .custom)
        btnadd.frame   = CGRect(x: 0, y: 0, width: 25, height: 25)
        btnadd.setImage(#imageLiteral(resourceName: "add"), for: UIControl.State.normal)
        btnadd.addTarget(self, action: #selector(addNewCategory), for: .touchUpInside)
        let rightBarButton : UIBarButtonItem = UIBarButtonItem(customView: btnadd)
        self.navigationItem.setRightBarButton(rightBarButton, animated: false)
    }
    
    //MARK: - Get stored data (from MainMenu)
    
    func retrieveData(){
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "MainMenu")
        do{
            arrayCategory = try managedObjectContext.fetch(fetchRequest) as! [MainMenu]
            collectionView.reloadData()
        }
            catch{
                print("Failed")
            }
    }
    
    //MARK: - Add new data (into MainMenu)
    
    @objc func addNewCategory(){
            let addCategoryVC = self.storyboard?.instantiateViewController(withIdentifier: "AddNewCategoryOrItemViewController") as! AddNewCategoryOrItemViewController
            self.navigationController?.pushViewController(addCategoryVC, animated: true)
    }
    
    //MARK: - UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayCategory.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! CategoryCollectionViewCell
        if let nameString = arrayCategory[indexPath.item].name{
            cell.labelCategory.text = nameString
        }
        if let imageData = arrayCategory[indexPath.item].image as Data? {
            if let image = UIImage(data:imageData) {
                cell.imageviewCategory.image =  image
            }
        }
        cell.buttonEdit.tag = indexPath.row
        cell.buttonEdit.addTarget(self, action: #selector(editEntry(sender:)), for: .touchUpInside)
        cell.buttonDelete.tag = indexPath.row
        cell.buttonDelete.addTarget(self, action: #selector(deleteEntry(sender:)), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat =  0
        let collectionViewSize = collectionView.frame.size.width - padding
        return CGSize(width: (collectionViewSize/2) - 20 , height: ((collectionViewSize/2) - 20) + (collectionViewSize * 0.166))
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return  UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    //MARK: - UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let menuOItemsVC = self.storyboard?.instantiateViewController(withIdentifier: "MenuItemTableViewController") as! MenuItemTableViewController
        menuOItemsVC.mainMenu = arrayCategory[indexPath.row]
        self.navigationController?.pushViewController(menuOItemsVC, animated: true)
    }
    
    //MARK: - Edit stored data (in MainMenu)
    
    @objc func editEntry(sender : UIButton){
        let addCategoryVC = self.storyboard?.instantiateViewController(withIdentifier: "AddNewCategoryOrItemViewController") as! AddNewCategoryOrItemViewController
        addCategoryVC.imageData = arrayCategory[sender.tag].image as Data?
        addCategoryVC.name = arrayCategory[sender.tag].name
        self.navigationController?.pushViewController(addCategoryVC, animated: true)
    }
    
    //MARK: - Delete stored data (from MainMenu)
    
    @objc func deleteEntry(sender : UIButton){
        if let nameString = arrayCategory[sender.tag].name {
            let predicateName = NSPredicate(format: "name = %@", nameString)
            let predicateImage = NSPredicate(format: "image = %@", arrayCategory[sender.tag].image!)
            fetchRequest.predicate = NSCompoundPredicate(type: .and, subpredicates: [predicateName, predicateImage])
        }
        do {
            let test = try managedObjectContext.fetch(fetchRequest) as! [MainMenu]
            if test.count > 0{
                managedObjectContext.delete(test[0])
                saveData()
            }
        }catch{
           GenericFunctions.showAlertView(targetVC: self, title: "Could not find data!", message: error.localizedDescription)
        }
    }
    
    //MARK: - Save data (into MainMenu)
    
    func saveData(){
        do{
            try managedObjectContext?.save()
            self.retrieveData()
        }catch{
            GenericFunctions.showAlertView(targetVC: self, title: "Could not save data!", message: error.localizedDescription)
        }
    }
}
