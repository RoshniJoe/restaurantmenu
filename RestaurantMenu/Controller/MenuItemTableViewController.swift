//
//  MenuItemTableViewController.swift
//  TouchCore
//
//  Created by Roshni Varghese on 2019-09-09.
//  Copyright © 2019 RoshniJoe. All rights reserved.
//

import UIKit
import CoreData

class MenuItemTableViewController: UITableViewController {
    
    var arrayItems = [MenuItems]()
    var entity : NSEntityDescription!
    var menuItem: MenuItems!
    var managedObjectContext : NSManagedObjectContext!
    var fetchRequest : NSFetchRequest<NSFetchRequestResult>!
    var mainMenu : MainMenu?
    
    //MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationBar()
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        managedObjectContext = appDelegate.persistentContainer.viewContext
        entity = NSEntityDescription.entity(forEntityName: "MenuItems", in: managedObjectContext)
        fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "MenuItems")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        retrieveData()
    }
    
    //MARK: - Design Nav Bar
    
    func setUpNavigationBar(){
        let titleLabel = UILabel()
        titleLabel.text = "Menu Items"
        titleLabel.font =  UIFont(name: "Arial-BoldMT", size: 17.0)
        titleLabel.textColor = #colorLiteral(red: 0.3647058824, green: 0.3607843137, blue: 0.3647058824, alpha: 1)
        titleLabel.textAlignment = .center
        let titlewidth = titleLabel.intrinsicContentSize.width
        titleLabel.frame = CGRect(x: 0, y: 0, width: titlewidth, height: 20)
        self.navigationItem.titleView = titleLabel
        let btnadd     = UIButton.init(type: .custom)
        btnadd.frame   = CGRect(x: 0, y: 0, width: 25, height: 25)
        btnadd.setImage(#imageLiteral(resourceName: "add"), for: UIControl.State.normal)
        btnadd.addTarget(self, action: #selector(addNewCategory), for: .touchUpInside)
        let rightBarButton : UIBarButtonItem = UIBarButtonItem(customView: btnadd)
        self.navigationItem.setRightBarButton(rightBarButton, animated: false)
    }
    
    //MARK: - Get stored data (from MenuItems)
    
    func retrieveData(){
        fetchRequest.predicate = NSPredicate(format: "main = %@", mainMenu!)
        do{
            arrayItems = try managedObjectContext.fetch(fetchRequest) as! [MenuItems]
            tableView.reloadData()
        }
        catch{
            print("Failed")
        }
    }
    
    //MARK: - Add new data (into MenuItems)
    
    @objc func addNewCategory(){
        let addCategoryVC = self.storyboard?.instantiateViewController(withIdentifier: "AddNewCategoryOrItemViewController") as! AddNewCategoryOrItemViewController
        addCategoryVC.fromMenuItem = true
        addCategoryVC.mainMenu = self.mainMenu
        self.navigationController?.pushViewController(addCategoryVC, animated: true)
    }
    
    //MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayItems.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemCell", for: indexPath) as! MenuItemTableViewCell
        if let nameString = arrayItems[indexPath.item].name{
            cell.labelItem.text = nameString
        }
        cell.labelPrice.text = String(format: "$%.2f", arrayItems[indexPath.item].price)
        if let imageData = arrayItems[indexPath.item].image as Data? {
            if let image = UIImage(data:imageData) {
                cell.imageviewItem.image =  image
            }
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    //MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let btn = UIButton()
        btn.tag = indexPath.row
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            self.deleteEntry(sender: btn)
            self.arrayItems.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        
        let edit = UITableViewRowAction(style: .default, title: "Edit") { (action, indexPath) in
            self.editEntry(sender: btn)
        }
        return [delete, edit]
    }
    
    //MARK: - Edit stored data (in MenuItems)
    
    @objc func editEntry(sender : UIButton){
        let addCategoryVC = self.storyboard?.instantiateViewController(withIdentifier: "AddNewCategoryOrItemViewController") as! AddNewCategoryOrItemViewController
        addCategoryVC.imageData = arrayItems[sender.tag].image as Data?
        addCategoryVC.price = arrayItems[sender.tag].price
        addCategoryVC.name = arrayItems[sender.tag].name
        addCategoryVC.fromMenuItem = true
        self.navigationController?.pushViewController(addCategoryVC, animated: true)
    }
    
    //MARK: - Delete stored data (from MenuItems)
    
    @objc func deleteEntry(sender : UIButton){
        if let nameString = arrayItems[sender.tag].name {
            let predicateName = NSPredicate(format: "name = %@", nameString)
            let predicateImage = NSPredicate(format: "image = %@", arrayItems[sender.tag].image!)
            let predicatePrice = NSPredicate(format: "price = %f", arrayItems[sender.tag].price)
            fetchRequest.predicate = NSCompoundPredicate(type: .and, subpredicates: [predicateName, predicateImage,predicatePrice])
        }
        do {
            let test = try managedObjectContext.fetch(fetchRequest) as! [MenuItems]
            if test.count > 0{
                managedObjectContext.delete(test[0])
                saveData()
            }
        }catch{
            GenericFunctions.showAlertView(targetVC: self, title: "Could not find data!", message: error.localizedDescription)
        }
    }
    
    //MARK: - Save data (into MenuItems)
    
    func saveData(){
        do{
            try managedObjectContext?.save()
            self.retrieveData()
        }catch{
            GenericFunctions.showAlertView(targetVC: self, title: "Could not save data!", message: error.localizedDescription)
        }
    }

}
