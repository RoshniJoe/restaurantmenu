//
//  AddNewCategoryOrItemViewController.swift
//  TouchCore
//
//  Created by Roshni Varghese on 2019-09-07.
//  Copyright © 2019 RoshniJoe. All rights reserved.
//

import UIKit
import CoreData

class AddNewCategoryOrItemViewController: UITableViewController , UITextFieldDelegate {
    
    @IBOutlet weak var imageviewCategory : UIImageView!
    @IBOutlet weak var textfieldCategory : UITextField!
    @IBOutlet weak var textfieldPrice : UITextField!
    @IBOutlet weak var toolbar : UIToolbar!

    var imagePicker = UIImagePickerController()
    var imageData : Data?
    var price : Double?
    var name : String?
    var entity : NSEntityDescription!
    var mainMenu: MainMenu!
    var managedObjectContext : NSManagedObjectContext!
    var fetchRequest : NSFetchRequest<NSFetchRequestResult>!
    var fromMenuItem : Bool?
    
    //MARK: - View Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        setUpNavigationBar()
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        managedObjectContext = appDelegate.persistentContainer.viewContext
        if fromMenuItem != nil{
            entity = NSEntityDescription.entity(forEntityName: "MenuItems", in: managedObjectContext)
            fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "MenuItems")
            textfieldCategory.placeholder = "Enter item name"
        }
        else{
            entity = NSEntityDescription.entity(forEntityName: "MainMenu", in: managedObjectContext)
            fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "MainMenu")
            textfieldPrice.isHidden = true
            textfieldCategory.placeholder = "Enter category name"

        }
        if name != nil{
            fetchDataToBeEdited()
        }
        textfieldPrice.inputAccessoryView = toolbar
    }
    
    //MARK: - Design Nav Bar
    
    func setUpNavigationBar(){
        let titleLabel = UILabel()
        titleLabel.text = "Add Category"
        titleLabel.font =  UIFont(name: "Arial-BoldMT", size: 17.0)
        titleLabel.textColor = #colorLiteral(red: 0.3647058824, green: 0.3607843137, blue: 0.3647058824, alpha: 1)
        titleLabel.textAlignment = .center
        let titlewidth = titleLabel.intrinsicContentSize.width
        titleLabel.frame = CGRect(x: 0, y: 0, width: titlewidth, height: 20)
        self.navigationItem.titleView = titleLabel
        let btnadd     = UIButton.init(type: .custom)
        btnadd.frame   = CGRect(x: 0, y: 0, width: 25, height: 25)
        btnadd.setImage(#imageLiteral(resourceName: "save"), for: UIControl.State.normal)
        btnadd.addTarget(self, action: #selector(insertIntoCoreData), for: .touchUpInside)
        let rightBarButton : UIBarButtonItem = UIBarButtonItem(customView: btnadd)
        self.navigationItem.setRightBarButton(rightBarButton, animated: false)
    }
    
    //MARK: - Add new data
    
    @objc func insertIntoCoreData(){
        if textfieldCategory.text != "" && textfieldCategory.text != nil
            && imageviewCategory.image != nil{
            if name == nil{ // edit data
                if fromMenuItem != nil{ // into Menu Items
                    if Double(textfieldPrice.text!) != nil{
                        let newMenuObject = MenuItems(entity: entity, insertInto: self.managedObjectContext)
                        newMenuObject.name = textfieldCategory.text!
                        newMenuObject.image = imageviewCategory.image!.pngData() as NSData?
                        newMenuObject.price = Double(textfieldPrice.text!)!
                        newMenuObject.main = mainMenu
                    }
                    else{
                        GenericFunctions.showAlertView(targetVC: self, title: "Alert", message: "Price is not valid")
                    }
                }
                else{ // into Main Menu
                    let newMenuObject = MainMenu(entity: entity, insertInto: self.managedObjectContext)
                    newMenuObject.name = textfieldCategory.text!
                    newMenuObject.image = imageviewCategory.image!.pngData() as NSData?
                }
                saveData()
            }
            else{ // new data
                if fromMenuItem != nil{ // into Menu Items
                    if let nameString = name {
                        fetchRequest.predicate = NSPredicate(format: "name = %@", nameString)
                    }
                    do {
                        let test = try managedObjectContext?.fetch(fetchRequest)
                        let objectUpdate = test?[0] as! MenuItems
                        objectUpdate.name = textfieldCategory.text!
                        objectUpdate.price = Double(textfieldPrice.text!)!
                        objectUpdate.image = imageviewCategory.image!.pngData() as NSData?
                        saveData()
                    }catch{
                        print(error)
                    }
                }
                else{ // into Main Menu
                    if let nameString = name {
                        fetchRequest.predicate = NSPredicate(format: "name = %@", nameString)
                    }
                    do {
                        let test = try managedObjectContext?.fetch(fetchRequest)
                        let objectUpdate = test?[0] as! MainMenu
                        objectUpdate.name = textfieldCategory.text!
                        objectUpdate.image = imageviewCategory.image!.pngData() as NSData?
                        saveData()
                    }catch{
                        print(error)
                    }
                }
            }
        }else{ // validate name & image
            let alert = UIAlertController(title: "Alert", message: "Kindly add image and name.", preferredStyle: UIAlertController.Style.alert)
            self.present(alert, animated: true, completion: nil)
            let when = DispatchTime.now() + 1
            DispatchQueue.main.asyncAfter(deadline: when){
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    //MARK: - Save data
    
    func saveData(){
        do{
            try managedObjectContext?.save()
            self.navigationController?.popViewController(animated: true)
        }catch{
            GenericFunctions.showAlertView(targetVC: self, title: "Could not save data!", message: error.localizedDescription)
        }
    }
    
    // Mark: - Textfield delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: Fetch data
    
    func fetchDataToBeEdited(){
        if let nameString = name {
            fetchRequest.predicate = NSPredicate(format: "name = %@", nameString)
        }
        if fromMenuItem != nil{ // from MenuItems
            do {
                let test = try managedObjectContext.fetch(fetchRequest) as! [MenuItems]
                if let nameString = test[0].name{
                    textfieldCategory.text = nameString
                }
                textfieldPrice.text = "\(test[0].price)"
                if let imageData = test[0].image {
                    if let image = UIImage(data:imageData as Data) {
                        imageviewCategory.image =  image
                    }
                }
            }catch{
                print(error)
            }
        }
        else{ // from Main Menu
            do {
                let test = try managedObjectContext.fetch(fetchRequest) as! [MainMenu]
                if let nameString = test[0].name{
                    textfieldCategory.text = nameString
                }
                if let imageData = test[0].image {
                    if let image = UIImage(data:imageData as Data) {
                        imageviewCategory.image =  image
                    }
                }
            }catch{
                print(error)
            }
        }
    }
    
    //MARK: - Action to pick image from camera or gallery
    
    @IBAction func actionPickImage(sender : Any){
        let actionSheet = UIAlertController(title: "Select from", message: nil, preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: "Camera", style: .default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.imagePicker.sourceType = .camera
                self.present(self.imagePicker, animated: true, completion: nil)
            } else {
                let alert = UIAlertController(title: "Alert", message: "No camera available.", preferredStyle: UIAlertController.Style.alert)
                self.present(alert, animated: true, completion: nil)
                let when = DispatchTime.now() + 1
                DispatchQueue.main.asyncAfter(deadline: when){
                    alert.dismiss(animated: true, completion: nil)
                }            }
        }
        actionSheet.addAction(camera)
        let gallery = UIAlertAction(title: "Gallery", style: .default) { (action) in
            self.imagePicker.sourceType = .savedPhotosAlbum
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        actionSheet.addAction(gallery)
        let cancel = UIAlertAction(title: "Cancel", style: .destructive) { (action) in
            
        }
        actionSheet.addAction(cancel)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction func actionDone(sender : Any){
        textfieldPrice.resignFirstResponder()
    }
}

//MARK: - UIPickerController delegate

extension AddNewCategoryOrItemViewController : UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.editedImage] as! UIImage
        imageviewCategory.image = image
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}
